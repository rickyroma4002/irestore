package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import java.net.URI;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LocationListener {
    LocationManager locationManager;
    TextView spot1, spot2, spot3, spot4, name, menuCibo, type;
    Button refresh, spot1bu, spot2bu, spot3bu, spot4bu, backToMenu;

    ImageView img;
    Point barCorni, barSelmi, machCorni, conad;
    Location loc;
    ConstraintLayout menu, selected;
    Point list[] = new Point[4];

    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        barCorni = new Point(2, "Bar Corni", "Bar", 10.888510964993275, 44.64806865709513);
        barSelmi = new Point(2, "Bar Selmi", "Bar", 10.888670028717783, 44.6479218004378);
        machCorni = new Point(1, "Distributore Corni", "Distributore", 10.888510964993275, 44.64806865709513);
        conad = new Point(1, "CONAD", "SuperMarket", 10.886783152949619, 44.64654410796579);

        refresh = (Button) findViewById(R.id.refresh);
        backToMenu = (Button) findViewById(R.id.backMenu);
        spot1 = (TextView) findViewById(R.id.spot1);
        spot2 = (TextView) findViewById(R.id.spot2);
        spot3 = (TextView) findViewById(R.id.spot3);
        spot4 = (TextView) findViewById(R.id.spot4);
        spot1bu = (Button) findViewById(R.id.spot1bu);
        spot2bu = (Button) findViewById(R.id.spot2bu);
        spot3bu = (Button) findViewById(R.id.spot3bu);
        spot4bu = (Button) findViewById(R.id.spot4bu);
        name = (TextView) findViewById(R.id.name);
        menuCibo = (TextView) findViewById(R.id.menuCibo);
        type = (TextView) findViewById(R.id.type);
        menu = (ConstraintLayout) findViewById(R.id.menu);
        selected = (ConstraintLayout) findViewById(R.id.sel);
        img = (ImageView) findViewById(R.id.imageView);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0 ,0,this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0 ,0,this);
        loc = get_loc();
        listAll();
        refresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loc = get_loc();
                listAll();
            }
        });
        spot1bu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeSceneToSel();
                name.setText("Name: "+list[0].name);
                type.setText("Type: "+list[0].type+"/ Number of Star: "+list[0].getStar());
                menuCibo.setText("bho");
                img.setImageResource(list[0].img);
                menuCibo.setText(list[0].menu);
            }
        });
        spot2bu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeSceneToSel();
                name.setText("Name: "+list[1].name);
                type.setText("Type: "+list[1].type+"/ Number of Star: "+list[1].getStar());
                menuCibo.setText("bho");
                img.setImageResource(list[1].img);
                menuCibo.setText(list[1].menu);
            }
        });
        spot3bu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeSceneToSel();
                name.setText("Name: "+list[2].name);
                type.setText("Type: "+list[2].type+"/ Number of Star: "+list[2].getStar());
                menuCibo.setText("bho");
                img.setImageResource(list[2].img);
                menuCibo.setText(list[2].menu);
            }
        });
        spot4bu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeSceneToSel();
                name.setText("Name: "+list[3].name);
                type.setText("Type: "+list[3].type+"/ Number of Star: "+list[3].getStar());
                menuCibo.setText("bho");
                img.setImageResource(list[3].img);
                menuCibo.setText(list[3].menu);
            }
        });
        backToMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeSceneToMenu();
            }
        });
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        loc = get_loc();
        listAll();
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        get_loc();
        listAll();
        LocationListener.super.onProviderEnabled(provider);
    }

    @Override
    public void onLocationChanged(@NonNull List<Location> locations) {
        LocationListener.super.onLocationChanged(locations);
    }

    @Override
    public void onFlushComplete(int requestCode) {
        LocationListener.super.onFlushComplete(requestCode);
    }

    public void listAll() {
        Point a;
        list[0] = barSelmi;
        list[1] = barCorni;
        list[2] = machCorni;
        list[3] = conad;
        for (int i = 0; i < list.length; i++) {
            boolean flag = false;
            for (int j = 0; j < list.length - 1; j++) {
                if (list[j].pos.distanceTo(loc) > list[j + 1].pos.distanceTo(loc)) {
                    a = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = a;
                    flag = true;
                }
            }
            if (!flag) break;
        }
        spot1.setText(list[0].name + "\n Distance: " + (int) list[0].pos.distanceTo(loc) + "m");
        spot2.setText(list[1].name + "\n Distance: " + (int) list[1].pos.distanceTo(loc) + "m");
        spot3.setText(list[2].name + "\n Distance: " + (int) list[2].pos.distanceTo(loc) + "m");
        spot4.setText(list[3].name + "\n Distance: " + (int) list[3].pos.distanceTo(loc) + "m");
    }

    public void changeSceneToMenu() {
        menu.setVisibility(View.VISIBLE);
        selected.setVisibility(View.INVISIBLE);
    }

    public void changeSceneToSel() {
        menu.setVisibility(View.INVISIBLE);
        selected.setVisibility(View.VISIBLE);
    }

    private Location get_loc() {
        boolean gps_enabled = false;
        boolean network_enabled = false;

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location net_loc = null, gps_loc = null, finalLoc = null;

        if (gps_enabled)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
        gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (network_enabled)
            net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (gps_loc != null && net_loc != null) {

            //smaller the number more accurate result will
            if (gps_loc.getAccuracy() > net_loc.getAccuracy())
                finalLoc = net_loc;
            else
                finalLoc = gps_loc;

            // I used this just to get an idea (if both avail, its upto you which you want to take as I've taken location with more accuracy)

        } else {

            if (gps_loc != null) {
                finalLoc = gps_loc;
            } else if (net_loc != null) {
                finalLoc = net_loc;
            }
        }
        return finalLoc;
    }
}