package com.example.myapplication;
import android.location.Location;
import android.location.LocationManager;

public class Point {
    private int star;
    public String name;
    public String indirizzo;
    public String type;
    public String menu;
    public int img;
    public Location pos = new Location(LocationManager.GPS_PROVIDER);
    public Point(int star, String name, String type, double lon, double lat){
        this.star=star;
        this.name=name;
        this.type=type;
        pos.setLongitude(lon);
        pos.setLatitude(lat);
        if (name.contains("Corni")){
            img=R.drawable.corni;
        }
        else if(name.contains("Selmi")){
            img=R.drawable.selmi;
        }
        else if(name.contains("CONAD")){
            img=R.drawable.conad;
        }
        if (type=="Bar"){
            menu="Normal energy bars, packets of potato chips with and without gluten," +
                    " soft drinks of all kinds and even still or sparkling water";
        }
        else if(type=="SuperMarket"){
            menu="Any kind of object or food, from fresh vegetables to prepackaged foods," +
                    "even equipped with an area for sausages and one for fresh fish," +
                    "in addition there is a refrigerator area for frozen foods. All this for a very modest price.";
        }
        else if(type=="Distributore"){
            menu="Stuffed sandwiches, croissants of all kinds, pizza, fried dumplings and wraps," +
                    "in addition you can buy drinks or bottles of still or sparkling water";
        }
    }

    public String getStar() {
        String s="";
        for(int i=0; i<star; i++){
            s+="+";
        }
        return s;
    }
}
